CC          := g++
CFLAGS      := -Wall -Wextra
SRCPATH     := .
INCPATH     := .
OBJPATH     := build/obj
DEPPATH 	:= build/dep
BINPATH     := build/bin
SRC         := $(wildcard $(SRCPATH)/*.cpp)
OBJ         := $(SRC:$(SRCPATH)/%.cpp=$(OBJPATH)/%.o)
DEP         := $(OBJ:$(OBJPATH)/%.o=$(DEPPATH)/%.d)
BIN         := $(BINPATH)/a.out
DIRS        := $(OBJPATH) $(DEPPATH) $(BINPATH)

$(shell mkdir -p $(DIRS))

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $^ -o $@

-include $(DEP)
$(OBJPATH)/%.o: $(SRCPATH)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I$(INCPATH) -MMD -MP -MF $(@:$(OBJPATH)/%.o=$(DEPPATH)/%.d)

.PHONY: clean run
clean:
	rm -rf $(DIRS)
run:
	$(BIN)

